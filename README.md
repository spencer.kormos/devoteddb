# Devoted DB

This application uses `go 1.12` and modules.

## To Run

1. Navigate to https://gitlab.com/spencer.kormos/devoteddb/blob/master/bin/devoteddb
1. Click the "Cloud" icon to download.
1. The binary is built to run on a Mac. If you're running this on a different OS you'll need to clone and build the project.
1. The binary is named `devoteddb`, it's readily executable with no parameters.

## Build

Steps:

```
$> git clone https://gitlab.com/spencer.kormos/devoteddb.git
$> cd $PATH_TO_PROJECT
$> go mod download
$> go build ./cmd/devoteddb/
$> ./devoteddb
```
