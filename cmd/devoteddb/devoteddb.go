package main

import (
	"bufio"
	"fmt"
	"gitlab.com/spencer.kormos/devoteddb/internal/database"
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/pkg/errors"

	"gitlab.com/spencer.kormos/devoteddb/internal/commands"
)

func main()  {
	fmt.Println("Welcome to the Devoted Database!")
	fmt.Print(">> ")

	cmds := initializeResources()

	for scanner := bufio.NewScanner(os.Stdin); scanner.Scan(); fmt.Print(">> ") {
		if err := scanner.Err(); err != nil {
			log.Fatal(errors.Wrap(err, "while reading from command line"))
		}

		parsedCommand := commandParser(scanner.Text())
		if len(parsedCommand) <= 0 {
			continue
		}

		requestedCommand := strings.ToUpper(parsedCommand[0])
		if cmd, ok := cmds[requestedCommand]; ok {
			if err := cmd(parsedCommand[1:]); err == commands.ErrExit {
				fmt.Println("Exiting...")
				break
			} else if err != nil {
				fmt.Println(errors.Wrapf(err, "while executing command %s", requestedCommand))
			}
		} else {
			fmt.Printf("Command not found. Use %s for a list of commands.\n", commands.NameHelp)
		}
	}
}

func commandParser(input string) []string {
	trimmed := strings.TrimSpace(input)
	if trimmed == "" {
		return []string{}
	}

	r := regexp.MustCompile(`[ ]{2,}`)
	trimmed = r.ReplaceAllString(trimmed, " ")

	return strings.Split(trimmed, " ")
}

func initializeResources() map[string]commands.CommandHandler {
	cmds := make(map[string]commands.CommandHandler)
	registerCommand(cmds, commands.ExitCommand())
	registerCommand(cmds, commands.HelpCommand())

	db := database.NewDatabase()
	registerCommand(cmds, commands.BeginCommand(db))
	registerCommand(cmds, commands.CommitCommand(db))
	registerCommand(cmds, commands.CountCommand(db))
	registerCommand(cmds, commands.DeleteCommand(db))
	registerCommand(cmds, commands.GetCommand(db))
	registerCommand(cmds, commands.RollbackCommand(db))
	registerCommand(cmds, commands.SetCommand(db))

	return cmds
}

type command interface {
	Name() string
	Executor() commands.CommandHandler
}

func registerCommand(commands map[string]commands.CommandHandler, cmd command) {
	commands[cmd.Name()] = cmd.Executor()
}
