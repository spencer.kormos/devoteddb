package database

import "github.com/pkg/errors"

var ERR_NO_TRANSACTIONS = errors.New("TRANSACTION NOT FOUND")

type record struct {
	deleted bool
	value string
	txId int
	prev *record
}

type db struct {
	store map[string]*record
	txId int
}

func NewDatabase() *db {
	return &db{
		store: make(map[string]*record),
		txId: 0,
	}
}

func (d *db) Begin() {
	d.txId++
}

func (d *db) Commit() {
	if d.txId == 0 {
		return
	}

	for name, rec := range d.store {
		if rec.deleted {
			delete(d.store, name)
			continue
		}

		rec.prev = nil
		rec.txId = 0
	}

	d.txId = 0

	//for name, rec := range d.store {
	//	if d.txId == 1 && rec.deleted {
	//		delete(d.store, name)
	//		continue
	//	}
	//
	//	if rec.txId == d.txId {
	//		rec.prev = rec.prev.prev
	//		rec.txId--
	//	}
	//}
	//
	//d.txId--
}

func (d *db) Count(value string) int {
	count := 0

	for _, r := range d.store {
		if !r.deleted && r.value == value {
			count++
		}
	}

	return count
}

func (d *db) Delete(name string) {
	if d.txId == 0 {
		delete(d.store, name)
		return
	}

	if rec, hasRecord := d.store[name]; hasRecord {
		if rec.txId == d.txId {
			rec.deleted = true
		} else {
			d.store[name] = &record{
				deleted: true,
				txId: d.txId,
				value: rec.value,
				prev: rec,
			}
		}
	}
}

func (d *db) Get(name string) string {
	if rec, hasRecord := d.store[name]; hasRecord && !rec.deleted {
		return rec.value
	}

	return "NULL"
}

func (d *db) Rollback() error {
	if d.txId == 0 {
		return ERR_NO_TRANSACTIONS
	}

	for name, rec := range d.store {
		if rec.txId == d.txId {
			if rec.prev == nil {
				delete(d.store, name)
				continue
			}

			d.store[name] = rec.prev
		}
	}

	d.txId--

	return nil
}

func (d *db) Set(name, value string) {
	rec, hasRecord := d.store[name]

	if hasRecord && rec.txId == d.txId {
		rec.value = value
		rec.deleted = false
		return
	}

	d.store[name] = &record{
		deleted: false,
		txId: d.txId,
		value: value,
		prev: rec,
	}
}
