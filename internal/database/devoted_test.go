package database_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/spencer.kormos/devoteddb/internal/database"
)

func TestDb_Example01(t *testing.T) {
	target := database.NewDatabase()

	assert.Equal(t, "NULL", target.Get("a"))

	target.Set("a", "foo")
	target.Set("b", "foo")
	assert.Equal(t, 2, target.Count("foo"))

	assert.Equal(t, 0, target.Count("bar"))

	target.Delete("a")
	assert.Equal(t, 1, target.Count("foo"))

	target.Set("b", "baz")
	assert.Equal(t, 0, target.Count("foo"))

	assert.Equal(t, "baz", target.Get("b"))

	assert.Equal(t, "NULL", target.Get("B"))
}

func TestDb_Example02(t *testing.T) {
	target := database.NewDatabase()

	target.Set("a", "foo")
	target.Set("a", "foo")
	assert.Equal(t, 1, target.Count("foo"))

	assert.Equal(t, "foo", target.Get("a"))

	target.Delete("a")
	assert.Equal(t, "NULL", target.Get("a"))
	assert.Equal(t, 0, target.Count("foo"))
}

func TestDb_Example03(t *testing.T) {
	target := database.NewDatabase()

	target.Begin()
	target.Set("a", "foo")
	assert.Equal(t, "foo", target.Get("a"))

	target.Begin()
	target.Set("a", "bar")
	assert.Equal(t, "bar", target.Get("a"))

	target.Set("a", "baz")
	assert.Nil(t, target.Rollback())

	assert.Equal(t, "foo", target.Get("a"))

	assert.Nil(t, target.Rollback())
	assert.Equal(t, "NULL", target.Get("a"))
}

func TestDb_Example04(t *testing.T) {
	target := database.NewDatabase()

	target.Set("a", "foo")
	target.Set("b", "baz")

	target.Begin()
	assert.Equal(t, "foo", target.Get("a"))

	target.Set("a", "bar")
	assert.Equal(t, 1, target.Count("bar"))

	target.Begin()
	assert.Equal(t, 1, target.Count("bar"))

	target.Delete("a")
	assert.Equal(t, "NULL", target.Get("a"))
	assert.Equal(t, 0, target.Count("bar"))

	assert.Nil(t, target.Rollback())
	assert.Equal(t, "bar", target.Get("a"))
	assert.Equal(t, 1, target.Count("bar"))

	target.Commit()
	assert.Equal(t, "bar", target.Get("a"))
	assert.Equal(t, "baz", target.Get("b"))
}
