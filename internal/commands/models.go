package commands

import "github.com/pkg/errors"

const NameBegin = "BEGIN"
const NameCommit = "COMMIT"
const NameCount = "COUNT"
const NameDelete = "DELETE"
const NameExit = "END"
const NameGet = "GET"
const NameHelp = "HELP"
const NameRollback = "ROLLBACK"
const NameSet = "SET"

var ErrExit = errors.New("EXIT")
var ErrParams = errors.New("incorrect number of arguments")

type CommandHandler func(params []string) error
