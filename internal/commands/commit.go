package commands

type committer interface {
	Commit()
}

type commitCmd struct{
	committer committer
}

func CommitCommand(committer committer) *commitCmd {
	return &commitCmd{
		committer: committer,
	}
}

func (c *commitCmd) Name() string {
	return NameCommit
}

func (c *commitCmd) Executor() CommandHandler {
	return func(params []string) error {
		if len(params) != 0 {
			return ErrParams
		}

		c.committer.Commit()
		return nil
	}
}
