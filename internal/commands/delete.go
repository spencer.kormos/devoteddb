package commands

type deleter interface {
	Delete(string)
}

type deleteCmd struct{
	deleter deleter
}

func DeleteCommand(deleter deleter) *deleteCmd {
	return &deleteCmd{
		deleter: deleter,
	}
}

func (d *deleteCmd) Name() string {
	return NameDelete
}

func (d *deleteCmd) Executor() CommandHandler {
	return func(params []string) error {
		if len(params) != 1 {
			return ErrParams
		}

		d.deleter.Delete(params[0])
		return nil
	}
}
