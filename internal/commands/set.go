package commands

type setter interface {
	Set(string, string)
}

type setCmd struct{
	setter setter
}

func SetCommand(setter setter) *setCmd {
	return &setCmd{
		setter: setter,
	}
}

func (s *setCmd) Name() string {
	return NameSet
}

func (s *setCmd) Executor() CommandHandler {
	return func(params []string) error {
		if len(params) != 2 {
			return ErrParams
		}

		s.setter.Set(params[0], params[1])
		return nil
	}
}
