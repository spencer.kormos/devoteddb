package commands

import "fmt"

type getter interface {
	Get(string) string
}

type getCmd struct{
	getter getter
}

func GetCommand(getter getter) *getCmd {
	return &getCmd{
		getter: getter,
	}
}

func (g *getCmd) Name() string {
	return NameGet
}

func (g *getCmd) Executor() CommandHandler {
	return func(params []string) error {
		if len(params) != 1 {
			return ErrParams
		}

		fmt.Println(g.getter.Get(params[0]))
		return nil
	}
}
