package commands

import "fmt"

type counter interface {
	Count(string) int
}

type countCmd struct{
	counter counter
}

func CountCommand(counter counter) *countCmd {
	return &countCmd{
		counter: counter,
	}
}

func (c *countCmd) Name() string {
	return NameCount
}

func (c *countCmd) Executor() CommandHandler {
	return func(params []string) error {
		if len(params) != 1 {
			return ErrParams
		}

		fmt.Println(c.counter.Count(params[0]))
		return nil
	}
}
