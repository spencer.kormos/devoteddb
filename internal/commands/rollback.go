package commands

import "fmt"

type rollbacker interface {
	Rollback() error
}

type rollbackCmd struct{
	rollbacker rollbacker
}

func RollbackCommand(rollbacker rollbacker) *rollbackCmd {
	return &rollbackCmd{
		rollbacker: rollbacker,
	}
}

func (r *rollbackCmd) Name() string {
	return NameRollback
}

func (r *rollbackCmd) Executor() CommandHandler {
	return func(params []string) error {
		if len(params) != 0 {
			return ErrParams
		}

		if err := r.rollbacker.Rollback(); err != nil {
			fmt.Println(err.Error())
		}
		return nil
	}
}
