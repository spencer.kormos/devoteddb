package commands

type beginner interface {
	Begin()
}

type beginCmd struct{
	beginner beginner
}

func BeginCommand(beginner beginner) *beginCmd {
	return &beginCmd{
		beginner: beginner,
	}
}

func (b *beginCmd) Name() string {
	return NameBegin
}

func (b *beginCmd) Executor() CommandHandler {
	return func(params []string) error {
		if len(params) != 0 {
			return ErrParams
		}

		b.beginner.Begin()
		return nil
	}
}
