package commands

type exitCmd struct{}

func ExitCommand() *exitCmd {
	return &exitCmd{}
}

func (e *exitCmd) Name() string {
	return NameExit
}

func (e *exitCmd) Executor() CommandHandler {
	return func(_ []string) error {
		return ErrExit
	}
}
