package commands

import "fmt"

type helpCmd struct{}

func HelpCommand() *helpCmd {
	return &helpCmd{}
}

func (h *helpCmd) Name() string {
	return NameHelp
}

func (h *helpCmd) Executor() CommandHandler {
	return func(_ []string) error {
		fmt.Println(
`
SET [name] [value]
      Sets the name in the database to the given value

GET [name]
      Prints the value for the given name. If the value is not in the database, prints NULL

DELETE [name]
      Deletes the value from the database

COUNT [value]
      Returns the number of names that have the given value assigned to them. If that value is not assigned anywhere, prints 0

END
      Exists the database

BEGIN
      Begins a new transaction

ROLLBACK
      Rolls back the most recent transcation. If there is no transcation to rollback, prints TRANSACTION NOT FOUND

COMMIT
      Commits all of the open transactions
`)
		return nil
	}
}
